            (function($){
                $(function(){
            
                $('.sidenav').sidenav();
                $('.user_profile_dropdown').dropdown({
                    coverTrigger: false
                });
                // sidebar__toggle
                $("#sidebar__toggle").click(function(e){
                    e.preventDefault();
                    $("body").toggleClass("active__minimal_sidebar");
                });
                }); // end of document ready
            })(jQuery); // end of jQuery name space
            //popup search modal 

			// //search tabs
			// var instance = M.Tabs.init(el);
			$(".dropdown-trigger").dropdown({
				coverTrigger: false,
				inDuration: 300,
				outDuration: 225,
				belowOrigin: true, // Displays dropdown below the button
				alignment: "left", // Displays dropdown with edge aligned to the left of button
			});
			//tabs
			$(document).ready(function () {
				$(".tabs").tabs();
				$(".sidenav").sidenav();

				// sidebar slide nav
				const myBody = document.querySelector("body");
				var hasNavItems = document.querySelectorAll(".has-sidenav");
				hasNavItems.forEach(function (navItem) {
					navItem.addEventListener('click', function () {
						const li = navItem.closest('li')
						const li_a = navItem.closest('li a')
						li.classList.add('slide-nav-open')
						li_a.classList.add ('active')
					
						myBody.addEventListener("click", function (e) {
							if (e.target !== li && !li.contains(e.target)) {
								li.classList.remove("slide-nav-open")
								li_a.classList.remove ('active')
							}
						});
					})
				});

				// Search bar hide show js
				var container = document.querySelector("li.liItem");
				var inputEle = document.querySelector("#searchInput");

				const html = document.querySelector("body");

				inputEle.addEventListener("click", function (e) {
					container.classList.add("show-dropdown");
				});

				html.addEventListener("click", function (e) {
					if (e.target !== inputEle && !container.contains(e.target)) {
						container.classList.remove("show-dropdown");
					}
				});

				//call tabs hide show js
				var container2 = document.querySelector("li.call");
				var inputEle2 = document.querySelector("#callInput");

				const body = document.querySelector("html");

				inputEle2.addEventListener("click", function (e) {
					container2.classList.add("show-dialpad");
				});

				html.addEventListener("click", function (e) {
					if (e.target !== inputEle2 && !container2.contains(e.target)) {
						container2.classList.remove("show-dialpad");
					}
				});


				//add content hide show js
				var container3 = document.querySelector("li.add");
				var inputEle3 = document.querySelector("#addInput");

				// const body = document.querySelector("html");

				inputEle3.addEventListener("click", function (e) {
					container3.classList.add("show-add");
				});

				html.addEventListener("click", function (e) {
					if (e.target !== inputEle3 && !container3.contains(e.target)) {
						container3.classList.remove("show-add");
					}
				});

				//Notifications hide show js
				var container4 = document.querySelector("li.notification");
				var inputEle4 = document.querySelector("#add_notification");

				// const body = document.querySelector("html");

				inputEle4.addEventListener("click", function (e) {
					container4.classList.add("show-notification");
				});

				html.addEventListener("click", function (e) {
					if (e.target !== inputEle4 && !container4.contains(e.target)) {
						container4.classList.remove("show-notification");
					}
				});

				//help hide show js
				var container5 = document.querySelector("li.help");
				var inputEle5 = document.querySelector("#add_help");

				// const body = document.querySelector("html");

				inputEle5.addEventListener("click", function (e) {
					container5.classList.add("show-help");
				});

				html.addEventListener("click", function (e) {
					if (e.target !== inputEle5 && !container5.contains(e.target)) {
						container5.classList.remove("show-help");
					}
				});


				//profile hide show js
				var container6 = document.querySelector("li.profile");
				var inputEle6 = document.querySelector("#add_profile");

				// const body = document.querySelector("html");

				inputEle6.addEventListener("click", function (e) {
					container6.classList.add("show-profile");
				});

				html.addEventListener("click", function (e) {
					if (e.target !== inputEle6 && !container6.contains(e.target)) {
						container6.classList.remove("show-profile");
					}
				});
			});